# TodoList

### 介绍

软件工程todolist学习demo

### 基础架构
前端：html、css、js
后端：Springboot
数据库：mysql，sql文件存放在static目录或public目录

### docker部署

#### 必要文件

* public文件夹中
  * todolist.sql
  * Dockerfile

* 自己创建
  * todolist.jar（注意数据库配置地址应为docker部署下的地址）

```shell
# 1. 拉取mysql镜像
docker pull mysql:8.0.31

# 2. 创建docker网络
docker network create -d bridge my-net

# 3. 创建容器mysql
docker run -p 3306:3306 --name mysql --network my-net \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/my.cnf \
-e MYSQL_ROOT_PASSWORD=123456demo \
-d mysql:8.0.31

# 4. 运行容器，创建todolist数据库导入sql
source todolist.sql

# 5 镜像构建
# 方法一：上传文件 Dockerfile，拉取源代码修改环境配置打包todolist.jar，在文件所在文件夹下构建镜像
docker build -t todolist:1.0 .

# 方法二：从已有的镜像构建
后来发现文件太大上传失败了...

# 6. 运行todolist
docker run -p 8080:8080 --name todolist --network my-net \
-d todolist:1.0
```
