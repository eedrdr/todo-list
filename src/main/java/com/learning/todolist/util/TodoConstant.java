package com.learning.todolist.util;

public interface TodoConstant {

    /**
     * 记录类型
     */
    int TYPE_ALL = 0;

    int TYPE_TODO = 1;

    int TYPE_DONE = 2;

}
