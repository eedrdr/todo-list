package com.learning.todolist.entity;

import lombok.Data;

@Data
public class Record {
    private int id;
    private String content;
    private int isDone;
    private int isDelete;
}
