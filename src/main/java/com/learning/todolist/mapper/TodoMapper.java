package com.learning.todolist.mapper;

import com.learning.todolist.entity.Record;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TodoMapper {

    /**
     * 添加记录
     */
    int insertRecord(String content);

    /**
     * 查询记录，可按已完成、未完成、所有实现查找
     */
    List<Record> selectRecords(int type);

    /**
     * 完成与已完成的切换
     */
    int updateDoneStatus(int id, int status);

    /**
     * 删除记录
     */
    int updateDeleteStatus(int id);

}
