package com.learning.todolist.controller;

import com.learning.todolist.entity.Record;
import com.learning.todolist.service.TodoService;
import com.learning.todolist.util.TodoConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TodoController implements TodoConstant {

    @Autowired
    private TodoService todoService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String root() {
        return "forward:index";
    }

    @RequestMapping(path = "index", method = RequestMethod.GET)
    public String getRecords(Model model){
        setModel(model);
        return "index";
    }

    /**
     * 添加记录
     */
    @GetMapping("add/{content}")
    public String addRecord(@PathVariable("content") String content, Model model){
        todoService.addRecord(content);
        setModel(model);
        return "index";
    }

    /**
     * 切换为已完成
     */
    @GetMapping("done/{id}")
    public String finishTask(@PathVariable("id") int id, Model model){
        todoService.updateDoneStatus(id, 1);
        setModel(model);
        return "index";
    }

    /**
     * 切换为未完成
     */
    @GetMapping("todo/{id}")
    public String todoTask(@PathVariable("id") int id, Model model){
        todoService.updateDoneStatus(id, 0);
        setModel(model);
        return "index";

    }

    /**
     * 删除记录
     */
    @GetMapping("delete/{id}")
    public String deleteRecord(@PathVariable("id") int id, Model model){
        todoService.updateDeleteStatus(id);
        setModel(model);
        return "index";
    }

    /**
     * 搜索记录并传给前端
     */
    private void setModel(Model model){
        List<Record> recordsList = todoService.findRecords(TYPE_ALL);
        List<Record> undoList = new ArrayList<>();
        List<Record> doneList = new ArrayList<>();
        for(Record record: recordsList){
            if (record.getIsDone() == 0){
                undoList.add(record);
            }else {
                doneList.add(record);
            }
        }
        model.addAttribute("undoList", undoList);
        model.addAttribute("doneList", doneList);
    }

}
