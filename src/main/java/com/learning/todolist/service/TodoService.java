package com.learning.todolist.service;

import com.learning.todolist.entity.Record;
import com.learning.todolist.mapper.TodoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    @Autowired
    private TodoMapper todoMapper;

    public int addRecord(String content){
        return todoMapper.insertRecord(content);
    }

    public List<Record> findRecords(int type){
        return todoMapper.selectRecords(type);
    }

    public int updateDoneStatus(int id, int status){
        return todoMapper.updateDoneStatus(id, status);
    }

    public int updateDeleteStatus(int id){
        return todoMapper.updateDeleteStatus(id);
    }
}
