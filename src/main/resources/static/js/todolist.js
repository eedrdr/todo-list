const todoInput = document.querySelector('#todoInput');
const add = document.querySelector('#add');
const todo =  document.querySelector('.todo');
const done =  document.querySelector('.done');
let xmlhttp=new XMLHttpRequest();
add.addEventListener('click', function(e){
    e.preventDefault();
    if(todoInput.value === ''){
        alert("您输入内容为空");
    }else{
        console.log(todoInput.value)
        xmlhttp.open("GET","add/"+todoInput.value, true)
        xmlhttp.send();
        location.reload();
    }
})

function click(e){
    let target = e.target
    let id = target.getAttribute("name");
    if(target.type === 'checkbox'){
        if(target.checked === true){
            xmlhttp.open("GET","done/"+id, true)
            xmlhttp.send();
        }else {
            xmlhttp.open("GET","todo/"+id, true)
            xmlhttp.send();
        }
    }else if(target.className === 'del'){
        xmlhttp.open("GET","delete/"+id, true)
        xmlhttp.send();
    }
    location.reload()
}
todo.addEventListener('click', click)
done.addEventListener('click', click)

