/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : todolist

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 01/01/2023 01:55:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for todolist
-- ----------------------------
DROP TABLE IF EXISTS `todolist`;
CREATE TABLE `todolist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_done` int DEFAULT NULL COMMENT '是否已完成',
  `is_delete` int DEFAULT NULL COMMENT '是否已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of todolist
-- ----------------------------
BEGIN;
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (1, '郊游', 1, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (2, '郊游2', 1, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (3, '吃饭', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (4, '吃饭', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (5, '吃饭', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (6, '睡觉', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (7, '考试', 1, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (8, '打豆豆', 1, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (9, '跑步', 1, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (10, '打游戏', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (11, '旅游', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (12, '做作业', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (13, '郊游', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (14, '郊游', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (15, '郊游', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (16, '郊游', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (17, '郊游', 0, 1);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (18, '睡觉', 1, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (19, '吃饭', 0, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (20, '郊游', 0, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (21, '郊游', 0, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (22, '郊游', 0, 0);
INSERT INTO `todolist` (`id`, `content`, `is_done`, `is_delete`) VALUES (23, '郊游', 0, 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
